import express from "express";
import path from "path";
import bodyParser from "body-parser";
import dotenv from "dotenv";

import auth from "./routes/auth";
import user from "./routes/user";

dotenv.config();
const app = express();
app.use(bodyParser.json());
app.use("/api/auth", auth);
app.use("/api/user", user);

app.get("/*", (req, res) => {
  res.sendFile(path.join(__dirname, "index.html"));
});

app.listen(3000, () => console.log("Running on localhost:3000"));
