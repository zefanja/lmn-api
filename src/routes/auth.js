import express from "express";
import jwt from "jsonwebtoken";
import ldap from "../connectors/ldap";
import authenticate from "../middleware/authenticate";

const router = express.Router();

// Helpers
function generateJWT(userDn) {
  return jwt.sign({ userDn }, process.env.JWT_SECRET);
}

function toAuthJSON(user) {
  return {
    ad: user,
    token: generateJWT(user.dn)
  };
}

// Routes
router.post("/", (req, res) => {
  const { credentials } = req.body;
  ldap
    .authenticate(credentials.username, credentials.password)
    .then(user => {
      if (user) {
        res.send({ user: toAuthJSON(user) });
      } else {
        res.status(400).send({ errors: { gobal: "Invalid credentials" } });
      }
    })
    .catch(err => {
      res.status(400).send({ errors: { global: err.message } });
    });
});

router.get("/current_user", authenticate, (req, res) => {
  res.json({
    user: {
      ad: req.currentUser,
      token: req.token
    }
  });
});

export default router;
