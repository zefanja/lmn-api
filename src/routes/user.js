import express from "express";
import csv from "csvtojson";
import { spawn } from "child_process";
import fs from "fs";
import ldap from "../connectors/ldap";
import {
  getJSON,
  skipProgress,
  checkForErrors,
  backupFile,
  removeLastLine,
  removeLine
} from "../utils/helper";

const router = express.Router();

const csvStudentsPath =
  "/etc/linuxmuster/sophomorix/default-school/students.csv";
const csvTeachersPath =
  "/etc/linuxmuster/sophomorix/default-school/teachers.csv";

router.get("/students", (req, res) => {
  csv({
    delimiter: [";"],
    noheader: true,
    headers: [
      "grade",
      "lastname",
      "firstname",
      "dateOfBirth",
      "identifier",
      "department"
    ]
  })
    .fromFile(csvStudentsPath)
    .then(response => {
      const studentsJSON = response
        .filter(e => e.grade[0] !== "#")
        .map((e, idx) => {
          e.id = idx;
          return e;
        });
      res.send({ students: studentsJSON });
    })
    .catch(e => {
      res.status(400).send({ errors: { global: e.message } });
    });
});

router.get("/teachers", (req, res) => {
  csv({
    delimiter: [";"],
    noheader: true,
    headers: [
      "unused",
      "lastname",
      "firstname",
      "dateOfBirth",
      "username",
      "initialPassword"
    ]
  })
    .fromFile(csvTeachersPath)
    .then(response => {
      const teachersJSON = response
        .filter(e => e.unused[0] !== "#")
        .map((e, idx) => {
          e.id = idx;
          return e;
        });
      res.send({ teachers: teachersJSON });
    });
});

router.get("/check", (req, res) => {
  const child = spawn("sophomorix-check", ["-jj"]);
  let output = "";

  child.on("exit", code => {
    if (code === 0) {
      res.send({ result: JSON.parse(getJSON(output)) });
    } else {
      res.status(400).send({ errors: { global: `exit with ${code}` } });
    }
  });

  child.stderr.on("data", data => {
    output += data;
  });
});

router.get("/add", (req, res) => {
  const child = spawn("sophomorix-add", ["-jj"]);
  let output = "";

  child.on("exit", code => {
    if (code === 0) {
      res.send({ result: JSON.parse(getJSON(output)) });
    } else {
      res.status(400).send({ errors: { global: `exit with ${code}` } });
    }
  });

  child.stderr.on("data", data => {
    output = skipProgress(output);
    output += data;
  });
});

router.get("/kill", (req, res) => {
  const child = spawn("sophomorix-kill", ["-jj"]);
  let output = "";

  child.on("exit", code => {
    if (code === 0) {
      res.send({ result: JSON.parse(getJSON(output)) });
    } else {
      res.status(400).send({ errors: { global: `exit with ${code}` } });
    }
  });

  child.stderr.on("data", data => {
    output = skipProgress(output);
    output += data;
  });
});

router.get("/update", (req, res) => {
  const child = spawn("sophomorix-update", ["-jj"]);
  let output = "";

  child.on("exit", code => {
    if (code === 0) {
      res.send({ result: JSON.parse(getJSON(output)) });
    } else {
      res.status(400).send({ errors: { global: `exit with ${code}` } });
    }
  });

  child.stderr.on("data", data => {
    output = skipProgress(output);
    output += data;
  });
});

function changePassword(username, newPassword) {
  return new Promise((resolve, reject) => {
    const child = spawn("sophomorix-passwd", [
      "-jj",
      "-u",
      username,
      "--pass",
      newPassword
    ]);
    let output = "";

    child.stderr.on("data", data => {
      output = skipProgress(output);
      output += data;
    });

    child.on("exit", code => {
      if (code === 0) {
        resolve({ result: JSON.parse(getJSON(output)), exit: code });
      } else {
        reject(Error({ errors: { global: `exit with ${code}` } }));
      }
    });
  });
}

router.post("/password", (req, res) => {
  // console.log(req.body, req.params);
  ldap
    .authenticate(req.body.username, req.body.password)
    .then(() => {
      changePassword(req.body.username, req.body.newPassword)
        .then(response => {
          if (response.exit === 0) {
            res.send({ changedPassword: true, result: response });
          } else {
            res.status(400).send({ errors: { global: response } });
          }
        })
        .catch(e => {
          res.status(400).send({ errors: { global: e.message } });
        });
    })
    .catch(err => {
      res.status(400).send({ errors: { global: err.message } });
    });
});

function sophomorixCheck() {
  return new Promise((resolve, reject) => {
    const child = spawn("sophomorix-check", ["-jj"]);
    let output = "";

    child.stderr.on("data", data => {
      output = skipProgress(output);
      output += data;
    });

    child.on("exit", code => {
      const result = JSON.parse(getJSON(output));
      const errors = checkForErrors(result);
      if (code === 0 && Object.keys(errors).length === 0) {
        resolve({ result, exit: code });
      } else {
        reject(Error(JSON.stringify(errors)));
      }
    });
  });
}

function sophomorixUpdate() {
  return new Promise((resolve, reject) => {
    const child = spawn("sophomorix-update", ["-jj"]);
    let output = "";

    child.stderr.on("data", data => {
      output = skipProgress(output);
      output += data;
    });

    child.on("exit", code => {
      const result = JSON.parse(getJSON(output));
      const errors = checkForErrors(result);
      if (code === 0 && Object.keys(errors).length === 0) {
        resolve({ result, exit: code });
      } else {
        reject(Error(JSON.stringify(errors)));
      }
    });
  });
}

function sophomorixAdd() {
  return new Promise((resolve, reject) => {
    const child = spawn("sophomorix-add", ["-jj"]);
    let output = "";

    child.stderr.on("data", data => {
      output = skipProgress(output);
      output += data;
    });

    child.on("exit", code => {
      if (code === 0) {
        resolve({ result: JSON.parse(getJSON(output)), exit: code });
      } else {
        reject(Error({ errors: { global: `exit with ${code}` } }));
      }
    });
  });
}

async function addStudent() {
  try {
    const resultCheck = await sophomorixCheck();
    const resultAdd = await sophomorixAdd();
    return {
      check: resultCheck,
      add: resultAdd
    };
  } catch (e) {
    throw Error(e.message);
  }
}

router.post("/student", (req, res) => {
  // Check if students has no errors
  sophomorixCheck()
    .then(() => {
      // if there are no errors, try to add a new student
      // Backup file first
      backupFile(csvStudentsPath);

      // Add new line
      const {
        grade,
        firstname,
        lastname,
        dateOfBirth,
        identifier
      } = req.body.student;
      const dataLine = `${grade};${lastname};${firstname};${dateOfBirth};${identifier}\n`;

      fs.appendFile(csvStudentsPath, dataLine, err => {
        if (err) {
          // append failed
          res.status(400).send({ errors: { global: "Couldn't add data!" } });
        } else {
          // done
          addStudent()
            .then(response => {
              res.send({ add: response.add, check: response.check });
            })
            .catch(e => {
              removeLastLine(csvStudentsPath);
              res.status(400).send({ errors: { global: e.message } });
            });
        }
      });
    })
    .catch(e => {
      res.status(400).send({ errors: { global: e.message } });
    });
});

router.delete("/student", (req, res) => {
  const { grade, firstname, lastname, dateOfBirth, identifier } = req.body;
  const dataLine = `${grade};${lastname};${firstname};${dateOfBirth};${identifier}`;

  backupFile(csvStudentsPath);
  removeLine(csvStudentsPath, dataLine)
    .then(() => {
      sophomorixCheck()
        .then(() => {
          sophomorixUpdate()
            .then(() => res.send({ removed: "Student removed!" }))
            .catch(e =>
              res.status(400).send({ errors: { global: e.message } })
            );
        })
        .catch(e => res.status(400).send({ errors: { global: e.message } }));
    })
    .catch(e => res.status(400).send({ errors: { global: e.message } }));
});

export default router;
