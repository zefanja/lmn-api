import { spawn } from "child_process";
import fs from "fs";
import readLastLines from "read-last-lines";

export const getJSON = data => {
	const tmp = data
		.replace(new RegExp("# JSON-begin", "g"), "")
		.replace(new RegExp("# JSON-end", "g"), "")
		.replace("\n", "");
	return tmp;
};

export const skipProgress = data => {
	if (data.search("PROGRESS") !== -1) {
		return "";
	}
	return data;
};

export const checkForErrors = data => {
	const result = data.CHECK_RESULT ? data.CHECK_RESULT : data;
	const errors = {};

	if (result.ERRORLIST && result.ERRORLIST.length !== 0) {
		errors.check = result.ERROR;
	}

	if (result.OUTPUT && result.OUTPUT.length !== 0) {
		result.OUTPUT.forEach(item => {
			if (item.TYPE === "ERROR") errors.output = result.OUTPUT;
		});
	}
	return errors;
};

export const backupFile = filename => {
	const backupFilePath = `${filename}.${new Date().valueOf()}`;
	spawn("cp", [filename, backupFilePath]);
};

export const removeLastLine = filename => {
	readLastLines.read(filename, 1).then(lines => {
		const toVanquish = lines.length;
		fs.stat(filename, (err, stats) => {
			if (err) throw err;
			fs.truncate(filename, stats.size - toVanquish, error => {
				if (error) throw error;
			});
		});
	});
};

export const removeLine = (filename, dataLine) =>
	new Promise((resolve, reject) => {
		fs.readFile(filename, "utf8", (err, data) => {
			if (err) reject(err);
			else {
				const lines = data.split("\n");
				const idx = lines.indexOf(dataLine);
				if (idx === -1) {
					reject(Error("Couldn't find line!"));
				} else {
					lines.splice(idx, 1);
					const linesToWrite = lines.join("\n");
					fs.writeFile(filename, linesToWrite, "utf8", error => {
						if (error) reject(error);
						else resolve();
					});
				}
			}
		});
	});
