import Ldap from "ldapjs-client";
import config from "../config";

const lmnClient = new Ldap({
  url: `ldap://${config.ldap.host}:${config.ldap.port}`
});

async function getUserDetails(userDn) {
  try {
    await lmnClient.bind(config.ldap.binddn, config.ldap.bindpw);
    const entries = await lmnClient.search(userDn, {});
    return entries[0];
  } catch (err) {
    throw Error("Couldn't find user with given DN!");
  }
}

async function lookupDN(username) {
  const opts = {
    filter: `(&(cn=${username})(objectClass=user))`,
    scope: "sub",
    attributes: ["dn"]
  };

  try {
    await lmnClient.bind(config.ldap.binddn, config.ldap.bindpw);
    const entries = await lmnClient.search(config.ldap.searchdn, opts);
    // console.log("lookupDN:", entries)
    if (entries[0].dn === "") throw Error("Couldn't find user!");
    else {
      return entries[0];
    }
  } catch (e) {
    throw Error("Couldn't bind global-binduser!");
  }
}

async function userObject(userDn, password) {
  try {
    // bind user
    await lmnClient.bind(userDn, password);
    // get user object
    const entries = await lmnClient.search(userDn, {});
    return entries[0];
  } catch (e) {
    throw Error("Couldn't get userObject");
  }
}

async function authenticate(username, password) {
  try {
    const userEntry = await lookupDN(username);
    const user = await userObject(userEntry.dn, password);

    return user;
  } catch (e) {
    throw Error(e.message);
  }
}

export default {
  userObject,
  lookupDN,
  authenticate,
  getUserDetails
};
