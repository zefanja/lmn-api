import jwt from "jsonwebtoken";
import ldap from "../connectors/ldap";

export default (req, res, next) => {
  const header = req.headers.authorization;
  let token;

  if (header) [, token] = header.split(" ");

  if (token) {
    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
      if (err) {
        res.status(401).json({ errors: { global: "Invalid token" } });
      } else {
        ldap.getUserDetails(decoded.userDn).then(user => {
          req.currentUser = user;
          req.token = token;
          next();
        });
      }
    });
  } else {
    res.status(401).json({ errors: { global: "No token" } });
  }
};
